# Les différentes possibilités de déploiement des applications Shiny

Maintenant que notre appliation Shiny est opérationnelle, il est temps de s'attaquer aux possibilités de déploiement. Nous introduirons ici trois moyens de déploiement :
- [le partage via le service Shinyapps.io](1_partage_application_shinyapps.io/README.md)
- [le partage sous forme de package R](2_creation_package_R_de_son_application/README.md)
- [le partage via ShinyProxy et l'utilisation des containers Docker](3_partage_application_shinyproxy/README.md)
