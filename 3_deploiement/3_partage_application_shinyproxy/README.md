# Partager son application via Shinyproxy et l'utilisation de containers Docker

## Principe de Shinyproxy

Shinyproxy est une application java qui permet de déployer ses applications Shiny sans aucune limite, que ce soit en nombre d'applications ou en nombre d'utilisateurs. La seule limite, ce sont les ressources que vous avez à votre disposition pour faire tourner ce service (CPU et RAM).

Le principe : l'utilisateur se connecte à votre Shinyproxy, il clique sur l'application Shiny qu'il souhaite exécuter, un container docker de l'application se lance et celle-ci s'affiche dans le browser de l'utilisateur. 

## Création de l’image docker de l’application Shiny à déployer

Je crée l’image docker de l'application Shiny à déployer sur shinyproxy. 

Je fais ça sur un serveur linux sur lequel Docker est installé. Note : je fais la construction en avance car l'installation de R et de ses packages est longue, donc la construction de l'image est longue.

**Structure du dossier à partir duquel je vais construire l'image Docker de l'application :**

<img src="images/1.png" height="100">

Il y a un Dockerfile pour construire l'image, la source de notre package contenant l'application Shiny et un fichier Rprofile.site.

**Contenu du Dockerfile :**

```docker
FROM rocker/r-ver

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update

## packages needed for basic shiny functionality
RUN Rscript -e "install.packages(c('shiny', 'ggplot2'),repos='https://cloud.r-project.org')"

# install the shiny application
RUN Rscript -e "install.packages('https://forgemia.inra.fr/inter_cati_omics/hackathon_shiny_2020/-/raw/master/3_deploiement/2_creation_package_R_de_son_application/hackathon_0.1.0.tar.gz',repos=NULL,type='source')"

# set host & port
COPY Rprofile.site /usr/lib/R/etc/
EXPOSE 3838

CMD ["R", "-e hackathon::shiny_application()"]
```

**Contenu du Rprofile.site :**

```
local({
   old <- getOption("defaultPackages")
   options(defaultPackages = c(old, "hackathon"), shiny.port = 3838, shiny.host = "0.0.0.0")
})
```

Note : Le Dockerfile permet d'installer R dans un environnement Linux, puis d'installer les packages "shiny" et "ggplot2" nécessaires au fonctionnement de notre application Shiny. Ensuite, il va copier la source de notre package R contenant l'application et va l'installer dans l'instance R disponible. Il copie le fichier Rprofile.site au bon endroit pour s'assurer que l'application Shiny sera lancée sur le port attendu par Shinyproxy, ici le port 3838. Il expose le port 3838 et enfin, il définit la commande à lancer lors de la création d'un container, soit la fonction permettant de lancer l'application Shiny.

**Construction de l'image Docker :**

Une fois ces 3 fichiers préparés, il suffit de se placer dans le dossier et d'exécuter la commande suivante : 

```sh
docker build -t avelt/hackathon .  
```

La construction de l'image étant très longue, dû à l'installation de l'ensemble des packages permettant de faire fonctionner Shiny et ggplot2, je vous conseille de récupérer directement l'image que j'ai généré et rendue disponible sur dockerhub.

**Partage de l'image sur Dockerhub :**

```sh
docker login
docker push avelt/hackathon
```

**Récupérer l'image via Dockerhub :** 

```sh
docker pull avelt/hackathon
```

## Déploiement de l'application Shiny avec Shinyproxy

### Lancement/configuration d'une instance sur Genouest

Pour cet atelier, je vais installer et configurer Shinyproxy sur une instance Genouest.

https://genostack.genouest.org/dashboard/project/instances/

Je choisi une instance Ubuntu 20.04 que je nomme hackathon:

<img src="images/2.png" height="200">

Je prends la version m2_xlarge, avec 4CPUs et 16Go de RAM :

<img src="images/3.png" height="500">

Je me connecte en ssh sur l'instance Genouest créée, avec l'utilisateur ubuntu :

```sh
ssh ubuntu@192.168.100.* 
```

**Note :**

Pour créer une instance sur Genouest, il faut un compte Genouest : https://my.genouest.org/manager2/register

De plus, il faut copier-coller votre clé publique locale sur genouest (https://my.genouest.org/manager2/user/ -> SSH) pour pouvoir s'y connecter. 

<img src="images/ssh.png" height="200">

Cette clé publique est disponible sur votre ordinateur dans ~/.ssh/id_rsa.pub. Cette technique marche avec Linux/MacOS, plus compliquée avec Windows -> installer WSL sur Windows pour avoir un LInux et simplifier la tâche.

Enfin, il faut se connecter à genossh.genouest.org, créer une clé publique (ssh-keygen), qui sera générée ici : ~/.ssh/id_rsa.pub. Puis importer cette clé publique sur genostack dans "paire de clés" : https://genostack.genouest.org/dashboard/project/key_pairs -> "Importer une clé publique" -> donner un nom et coller la clé publique dans "Clé publique *"

Ensuite, pour accéder à la VM : 

```sh
ssh avelt@genossh.genouest.org
ssh ubuntu@192.168.100.*
```

Note : lors de la création de l'instance, une IP est générée et est disponible sur l'interface dans la partie "instances". Le "*" correspond au nombre attribué à votre instance et à l'IP complète de votre instance pour vous y connecter.

### Installation de Docker sur l'instance Genouest

J'installe Docker sur cette instance en suivant le mode op' proposé par Docker (https://docs.docker.com/engine/install/ubuntu/) : 

```sh
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

J'ajoute le user ubuntu au groupe docker pour ne pas avoir à lancer les commandes docker en root :

```sh
sudo usermod -a -G docker ubuntu
```

Note : il faut se déconnecter et se reconnecter pour appartenir au groupe docker.

### Installation de Shinyproxy sur l'instance Genouest

Je récupère le Dockerfile et le fichier application.yml.

```sh
mkdir shinyproxy
cd shinyproxy
wget https://forgemia.inra.fr/inter_cati_omics/hackathon_shiny_2020/-/raw/master/3_deploiement/3_partage_application_shinyproxy/shinyproxy/Dockerfile
wget https://forgemia.inra.fr/inter_cati_omics/hackathon_shiny_2020/-/raw/master/3_deploiement/3_partage_application_shinyproxy/shinyproxy/application.yml
```

**application.yml** : 

```yml
proxy:
  port: 80
  container-wait-time: 30000
  authentication: simple
  admin-groups: admins
  users:
  - name: avelt
    password: avelt
    groups: admins
  - name: user1
    password: user1_pass
  - name: user2
    password: user2_pass
  - name: user3
    password: user3_pass
  docker:
      internal-networking: true
  specs:
  - id: hackathon
    description: Ma première application
    container-cmd: ["R", "-e", "hackathon::shiny_application()"]
    container-image: avelt/hackathon
    container-network: sp-example-net
  - id: 01_hello
    display-name: Hello Application
    description: Application which demonstrates the basics of a Shiny app
    container-cmd: ["R", "-e", "shinyproxy::run_01_hello()"]
    container-image: openanalytics/shinyproxy-demo
    container-network: sp-example-net
  - id: 06_tabsets
    container-cmd: ["R", "-e", "shinyproxy::run_06_tabsets()"]
    container-image: openanalytics/shinyproxy-demo
    container-network: sp-example-net
logging:
  file:
    shinyproxy.log
```

Il s'agit de la configuration de Shinyproxy. Shinyproxy sera disponible sur le port 80. 

Ici il s'agit d'une authentification simple, on met directement les noms d'utilisateurs et mot de passe dans le fichier de configuration. Une documentation détaillée de tous les systèmes d'authentifications supportées par Shinyproxy est disponible ici : https://www.shinyproxy.io/configuration/#authentication

Enfin, on spécifie les applications Shiny qu'on souhaite héberger sur ce Shinyproxy, en donnant notamment l'image docker de l'application et la commande lancée dans le container pour exécuter cette application.

**Dockerfile** :

```docker
FROM openjdk:11-jre

RUN mkdir -p /opt/shinyproxy/
RUN wget https://www.shinyproxy.io/downloads/shinyproxy-2.4.1.jar -O /opt/shinyproxy/shinyproxy.jar
COPY application.yml /opt/shinyproxy/application.yml

WORKDIR /opt/shinyproxy/

EXPOSE 80

CMD ["java", "-jar", "/opt/shinyproxy/shinyproxy.jar"]
```

Il s'agit du Dockerfile fourni par Shinyproxy, qui part de l'image openjdk qui rend disponible java 11 avec l'OS Debian -> Shinyproxy est une application Java. Puis ce Dockerfile va permettre de construire une image contenant Shinyproxy : pour ça, on récupère le .jar de Shinyproxy ainsi que la configuration de Shinyproxy qu'on a créé (application.yml). On expose le port 80 (port sur lequel écoutera le container) et la commande lancée dans le container sera l'exécution du .jar de Shinyproxy.

Créer un réseau docker que Shinyproxy va utiliser pour communiquer avec les containers Shiny :

```sh
docker network create sp-example-net
```

Je construit l'image Shinproxy :

```sh
docker build -t shinyproxy .
```

Je récupère l'image de mon application Shiny et des deux applications Shiny exemple proposées par Shinyproxy :

```sh
docker pull avelt/hackathon
docker pull openanalytics/shinyproxy-demo
```

### Lancement de Shinyproxy sur l'instance Genouest

Je lance Shinyproxy sur le port 80

```sh
sudo docker run -d -v /var/run/docker.sock:/var/run/docker.sock --net sp-example-net -p 80:80 shinyproxy
```

Maintenant, pour se connecter à Shinyproxy : 

https://hackathon-192-168-100-*.vm.openstack.genouest.org/


Note : lors de la création de l'instance, une IP est générée et est disponible sur l'interface dans la partie "instances". Le "*" correspond au nombre attribué à votre instance et à l'IP complète de votre instance pour vous y connecter.

Se connecter à Shinyproxy avec l'utilisateur exemple user1/user1_pass.

### Mise en page de l'interface de Shinyproxy

Par défault, Shinyproxy présente les applications disponibles sous la forme d'une simple liste. Il est toutefois possible d'améliorer cette présentation :
- On peut définir un titre et un logo en ajoutant les champs `title` et `logo-url` dans le fichier application.yml.
- En suivant cet exemple (https://github.com/openanalytics/shinyproxy-config-examples/tree/master/04-custom-html-template), on peut ajouter un template personalisé. Ceci modifie l'apparence de ShinyProxy dans le navigateur. Pour cela, il faut :
  - Ajouter un dossier template, contenant a minima un index.html. On peut aussi ajouter une CSS et des images par exemple.
  - Pointer vers le dossier tempate (où est présent index.html) avec le champ `template-path` dans le fichier application.yml.
  - Embarquer le dossier *template* lors de la création de l'image docker en ajoutant la ligne `COPY templates /opt/shinyproxy/templates`

Dans l'exemple précédent, un tableau des applications disponibles est construit avec les vignettes de chaque application. 
Voici un exemple de page d'accueil d'un server Shinyproxy qui utilise de cette personalisation : https://sunshine.irstea.fr/

Après chaque modification, il nécéssaire de tuer le shinyproxy en cours d'exécution (on récupère son ID avec `docker ps`), re-constrire l'image Docker, puis de lancer l'image nouvellement construite:
```sh
docker kill <CONTAINER_ID>
docker build --no-cache -t shinyproxy . 
docker run -d -v /var/run/docker.sock:/var/run/docker.sock --net sp-example-net -p 80:80 shinyproxy
```
