# Hackathon Shiny du inter-CATI omics 

Vous êtes sûrement déjà à l’aise avec R pour analyser vos données et les représenter graphiquement. Le package Shiny permet de créer des applis web interactives à partir de scripts R. Ainsi vous pouvez partager facilement vos développements avec des interlocut·eur·rice·s néophytes. Lors de cet atelier, nous découvrirons ensemble les principes de bases de Shiny puis nous vous accompagnerons avec un tutoriel pratique. Pour celles et ceux qui le souhaitent, nous aborderons également les solutions de déploiement d'appli Shiny grâce aux solutions shinyapps.io et ShinyProxy (docker). Cet atelier servira de base pour que chacun·e puisse participer au prochain en transformant l'un de ses scripts R en appli Shiny.

* Animateurs : Cédric Midoux & Amandine Velt
* Helpers : Thomas Denecker, Jonathan Kreplak, Arthur Péré, Andrea Rau 

## Pré-requis

Pour le bon déroulement du TP, merci de suivre les installations suivantes :

* [R](https://rstudio.com/products/rstudio/download/#download)
* [RStudio](https://rstudio.com/products/rstudio/download/#download)
* Les packages `install.packages(c("shiny", "ggplot2", "devtools", "rsconnect"))`
* Pour les utilisateurs de Windows, [RTools](http://jtleek.com/modules/01_DataScientistToolbox/02_10_rtools/#1) vous servira pour la partie deploiement d'applications Shiny sous forme de package que vous pourrez réaliser après le hackathon

## Déroulement 

* 9h-10h : [Exposé introductif](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/1_slides/slides.pdf) ([captation live disponible :video_camera:](https://genobbb.inrae.fr/playback/presentation/2.0/playback.html?meetingId=af986b8552f7b4e561509c8bd3aec210fadea71e-1604388776754 ))
* 10h-12h et 14h-15h : [Exercices pratiques](2_exercices/README.md) 
* 15h-16h : [Démonstrations de déploiements d'applications](3_deploiement/README.md) (pour les intéressés)
* 16h-16h45 : suite TP ou TP déploiement 
* 16h45-17h : Retours sur la journée, échanges et conclusion

## Documentation

* [Site officiel](https://shiny.rstudio.com/) 
* [Tutoriels vidéo](https://shiny.rstudio.com/tutorial/)
* [Cheat sheet](https://shiny.rstudio.com/images/shiny-cheatsheet.pdf)
* Gallery [officielle](https://shiny.rstudio.com/gallery/) et de la [communauté](http://www.showmeshiny.com/) (actuellement en refonte)
* Documentation spécifique au solutions de déploiements : [shinyapps.io](https://docs.rstudio.com/shinyapps.io/) et [ShinyProxy](https://shinyproxy.io/documentation/getting-started/)
* [Dépôt de la formation IFB](https://github.com/IFB-ElixirFr/IFB_Shiny_training)
* Billet de blog introductif en français : [bioinfo-fr](https://bioinfo-fr.net/rendre-ses-projets-r-plus-accessibles-grace-a-shiny), [super statisticienne](https://superstatisticienne.fr/r-sur-le-web-le-package-shiny/) et [ThinkR](https://thinkr.fr/a-decouverte-de-shiny/)

## Liens partagés lors de la journée 

* [De belles couleurs](https://cran.r-project.org/web/packages/colourpicker/vignettes/colourpicker.html)
* [Des jolis boutons](https://github.calpineponchoom/dreamRs/shinyWidgets)
* [Un joli dashboard](https://rinterface.com/shiny/shinydashboardPlus/)
* [Des jolis tableaux réactifs](https://rstudio.github.io/DT/shiny.html)
* [Exemple application RNAseq](http://shiny.imbei.uni-mainz.de:3838/ideal/)
