# 1. Shiny, c'est parti !

* Créer une application shiny avec RStudio `File` > `New File` > `Shiny Web App`

* Donner un nom à votre première appli et enregistrer la dans le dossier de votre choix avec le choix "Single File"

* Parcourir et exécuter le code :

```R
#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("Old Faithful Geyser Data"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            sliderInput("bins",
                        "Number of bins:",
                        min = 1,
                        max = 50,
                        value = 30)
        ),

        # Show a plot of the generated distribution
        mainPanel(
           plotOutput("distPlot")
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

    output$distPlot <- renderPlot({
        # generate bins based on input$bins from ui.R
        x    <- faithful[, 2]
        bins <- seq(min(x), max(x), length.out = input$bins + 1)

        # draw the histogram with the specified number of bins
        hist(x, breaks = bins, col = 'darkgray', border = 'white')
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
```

> :memo: [app.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app.R)

# 2. Ajouter du texte

* Sous la figure, ajouter une phrase précisant le nombre de bins.

> :memo: [app2.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app2.R)

# 3. Choisir un titre

* Ajouter un champs texte pour laisser à l'utilisateur le choix du titre de l'application `titlePanel`.

> :memo: [app3.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app3.R)

# 4. Un peu de couleur

* Ajouter des boutons radio permettant de choisir parmi les couleurs `"darkgray"`, `"coral"`, `"lightgreen"`.

> :memo: [app4.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app4.R)

# 5. De nouvelles données

* Actuellement notre plot affiche les données des durées entre deux éruptions du geyser *Old Faithful*.
* Les données sont directement gérées au sein de la fonction du plot.
* On va chercher a séparer le côté données, du côté figures.
* Supprimer la ligne correspondante (`x    <- faithful[, 2]`) de la fonction et la remplacer par un appel à l'expression réactive `datasetInput()` (`x    <- datasetInput()`) qui permettra de gérer les données de manière dynamique et réactive. Avant le `renderPlot` on définit l'expression reactive *datasetInput* tel que :

```R
datasetInput <- reactive({faithful$waiting})
```

* Tester l'application, verifier que tout fonctionne toujours. Essayer de comprendre et d'assimiler l'articulation des différents éléments.
* Maintenant que tout est bon, avec une liste déroulante, proposer le choix entre la durée des éruptions du geyser *Old Faithful* (`faithful$waiting`), la longueur des sépales des iris de *Fisher* (`iris$Sepal.Length`) ou la puissance des voitures d'après *Motor Trend* (`mtcars$hp`) et mettre à jour `datasetInput()` en fonction de ce choix.
  * **Note** : On peut utiliser un enchainement de `else if` ou bien la fonction `switch`

> :memo: [app5.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app5.R)

# 6. Des données générées aléatoirement

* Ajouter un choix dans la liste correspondant à un tirage aléatoire de 100 valeurs distribuées normalement.
  * Ajouter un champ dédié dans la liste déroulante
  * Mettre à jour les données pour renvoyer `rnorm(100)` si  *random* est sélectionné dans le champ *data*.

> :memo: [app6.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app6.R)

# 7. Paramètres du tirage aléatoire

* Dans la barre de paramètres, ajouter une section dédiée aux paramètres du tirage (`n`, `mean`, `sd`).
* **Bonus** : Afficher ces paramètres uniquement lorsque l'on sélectionne les données aléatoires dans la liste déroulante (voir `conditionalPanel`).
* **Note** : Le `reactive()` ne dépend pas des inputs du choix de couleur ou du nombre de *bins*. Le tirage aléatoire ne change donc pas lorsque la couleur ou les *bins* sont modifiés. 

> :memo: [app7.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app7.R)

# 8. Upload de données

* Permettre à l'utilisateur de charger ses données au format csv.
  * Ajouter un champ dédié dans la liste déroulante
  * Ajouter un widget d'upload
  * **Aide R** : Utiliser la fonction `read.csv(file, header = FALSE)[,1]` pour extraire la première colonne.
  * **Bonus** : Anticiper l'erreur du fichier non sélectionné avec les fonctions `validate()` et `need()` anisi que l'opérateur booléen OR `||`

> :memo: [app8.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app8.R)  
> :memo: [app8bonus.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app8bonus.R)

# 9. Plusieurs onglets

* Ajouter des onglets dans le `mainPanel()` avec l'aide de la fonction `tabsetPanel()`
  * Le premier onglet : un histogramme
  * Le second onglet : un boxplot
  * Le troisième onglet : un tableau des données

> :memo: [app9.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app9.R)

# 10. Des data.frames et `ggplot2` pour des graphiques élégants

* `ggplot2` comme de nombreux autres packages R utilise les dataframes. On va donc exploiter nos données avec ce format.
  * Pour les données de démo `faithful`, `iris` et `mtcars`, ce sont des dataframes
  * Pour les données chargées par l'utilisateur, il s'agit d'un dataframe avant que l'on extrait la première colonne
  * Pour les données générées aléatoirement on va construire sous le format data.frame
* Remplacer le reactive `datasetInput` par :

```R
datasetInput <- reactive({
    switch(input$data,
           "faithful" = faithful,
           "iris" = iris,
           "mtcars" = mtcars,
           "random" = data.frame("rnorm" = rnorm(n = input$randomN, mean = input$randomMean, sd = input$randomSd)),
           "upload" = read.csv(input$file$datapath, header = TRUE))
})
```

* Ajouter un widget `varSelectInput` destiné à sélectionner une variable.
  * Pour que ce widget se mette à jour en fonction des données choisies, il est nécessaire de le placer côté server.
  * Pour l'afficher coté UI, on utilisera `uiOutput` qui renverra vers le `renderUI` associé où sera placé le widget.
* Remplacer l'histogramme et le boxplot par l'équivalent en ggplot2. Le sujet du TP n'étant pas spécialement l'utilisation de `ggplot2`, voici directement les fonctions. Si vous êtes à l'aise, laissez libre cours à votre imagination pour les personnaliser.
  * Histogramme : `ggplot(data = datasetInput(), aes_string(x = input$variable)) + geom_histogram(bins = input$bins, fill = input$color, color = "white")`
  * Boxplot : `ggplot(data = datasetInput(), aes_string(y = input$variable)) + geom_boxplot(fill = input$color)`

> :memo: [app10.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app10.R)

# 11. Téléchargement d'un plot

* Ajouter un bouton pour télécharger le plot grâce aux fonctions `downloadHandler` et `ggsave`.

**Note** : Pour que le téléchargement se lance correctement, lancer l'application dans un vrai browser. Utiliser la commande : 

```r
runApp('app11.R', launch.browser = TRUE) 
```

> :memo: [app11.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app11.R)

# 12. Mise en forme avec CSS

* On peut facilement mettre en forme l'interface utilisateur d'une application avec CSS (*cascading style sheets*).

  * Choisir une feuille de style sur [Bootswatch](https://bootswatch.com/)
  * Télécharger le fichier `bootstrap.css` dans le dossier `www/` de votre appli
  * Indiquer le thème avec `theme = "bootstrap.css"` dans la fonction `fluidPage()`

> :memo: [app12.R](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/app12.R) avec un fichier [bootstrap.css](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/2_exercices/correction/www/bootstrap.css) dans le dossier `www/`

# 13. Déploiement d'application avec shinyapps.io

* Voir la [section dédiée](../3_deploiement/1_partage_application_shinyapps.io/README.md)