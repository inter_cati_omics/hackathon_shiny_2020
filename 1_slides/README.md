# Exposé introductif

{% pdf title="Développement d’une application web interactive avec R et le package Shiny", src="slides.pdf", width="100%", height="550", link=true %}{% endpdf %}

:bar_chart: Les supports de la présentation sont disponibles au format [PDF](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/1_slides/slides.pdf) et [PPTX](https://inter_cati_omics.pages.mia.inra.fr/hackathon_shiny_2020/1_slides/slides.pptx) sous licence libre GPL3.
